import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.authenticateUser(); // a method like this will be called on Init level to prevent the user to navigate between routes without authentication.
  }

  authenticateUser() {
    let role = this.authenticationService.getRole();
    switch(role) {
      case ("admin").valueOf():
        this.router.navigate(['/home']);
        break;
      default:
        this.router.navigate(['/login']);
    }
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

}
