import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  hide: boolean = true;
  loginErrorMessage: string = "";
  loggedUser: User;

  constructor(private router: Router, private userService: UserService, private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.loggedUser = new User();
  }

  login(event: any): void {
    event.preventDefault();
    this.loggedUser = this.userService.getUserByUsername(this.username);
    if (this.loggedUser.password == this.password) {
      this.authenticationService.login(this.loggedUser);
      let role = this.authenticationService.getRole();
      
      switch(role) {
        case ("admin").valueOf():
          this.router.navigate(['/home']);
          break;
        default:
          this.router.navigate(['/login']);
      }
    } else {
      this.loginErrorMessage = "Incorrect credentials. Please try again."
    }

  }

  register(event: any): void {
    event.preventDefault();
    this.router.navigate(['/register'])
  }

}
