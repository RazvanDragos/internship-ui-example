import { Injectable } from '@angular/core';
import { User } from 'src/app/models/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: User = {
      firstName: "Razvan",
      lastName: "Dragos",
      userName: "r.dragos",
      password: "parola",
      role: "admin"
    }

  constructor() { }

  getUserByUsername(userName: string) {
    return this.user;
  } //this is just a mock, you will use your REST GET method to get the User by a property, ex: userName
}
