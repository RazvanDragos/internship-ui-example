import { Injectable } from '@angular/core';
import { User } from 'src/app/models/User';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor() { }

  login(user: User): void {
    sessionStorage.setItem('currentUser', JSON.stringify({
      firstName: user.firstName,
      lastName: user.lastName,
      username: user.userName,
      password: user.password,
      role: user.role
    }));  
  }

  logout(): void {
    sessionStorage.removeItem('currentUser');
  }

  public getRole(): string {
    if (sessionStorage.getItem('currentUser') == null) {
      return '';
    }
    return JSON.parse(sessionStorage.getItem('currentUser'))['role'];
  } //same methods will be generated for rest of user properties: firstName, lastName etc

}
