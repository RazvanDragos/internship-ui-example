# InternshipDemo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.4.

## Application dependencies

Please git clone this repository on your local machine.

Import this project into your Visual Studio Code.

In the CLI
    - run `npm install` so that the project fetches its dependencies.
    - run `npm start` to start the application on port 4200.


